//
//  ViewController.swift
//  SwiftTest
//
//  Created by Frost on 05.10.15.
//  Copyright © 2015 Igor. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet var username: NSTextField!
    @IBOutlet var password: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func usernameTextField() -> String {
        let defVal = "none"
        
        if username == nil {
            return defVal
        } else {
            return username.stringValue
        }
    }
    
    func passwordTextFiled() -> String {
        let defVal = "none"
        
        if password == nil {
            return defVal
        } else {
            return password.stringValue
        }
    }
}

