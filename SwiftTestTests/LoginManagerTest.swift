//
//  LoginManagerTest.swift
//  SwiftTest
//
//  Created by Frost on 05.10.15.
//  Copyright © 2015 Igor. All rights reserved.
//

import XCTest
@testable import SwiftTest

class LoginManagerTest: XCTestCase {

    let loginManager = LoginManager()
    
    let mainController = ViewController()
    
    
    func test_login() {
        let username = mainController.usernameTextField()
        
        let pass = mainController.passwordTextFiled()

        XCTAssertTrue(loginManager.login(username, password: pass))
    }
}
