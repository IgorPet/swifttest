//
//  SwiftTestUITests.swift
//  SwiftTestUITests
//
//  Created by Frost on 05.10.15.
//  Copyright © 2015 Igor. All rights reserved.
//

import XCTest

@testable import SwiftTest

class SwiftTestUITests: XCTestCase {
    
    
    let loginManager = LoginManager()
    
    let mainController = ViewController()


    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let window = XCUIApplication().windows["Window"]
        let loginTextField = window.textFields["Login"]
        loginTextField.click()
        loginTextField.typeText("igor")
        window.textFields["Password"].click()
        window.textFields["Password"].typeText("password")
        
        let username = mainController.usernameTextField()
        
        let pass = mainController.passwordTextFiled()
        
        XCTAssertTrue(loginManager.login(username, password: pass))
//        test_login()
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
